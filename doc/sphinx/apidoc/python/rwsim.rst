rwsim module
============

.. automodule:: rwsim
    :members:
    :undoc-members:
    :show-inheritance:
    :special-members: __init__
