rw module
=========

.. automodule:: rw
    :members:
    :undoc-members:
    :show-inheritance:
    :special-members: __init__
