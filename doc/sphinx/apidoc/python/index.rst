.. _api_python:

********************
Python API Reference
********************

.. toctree::
   :maxdepth: 2

   rw.rst
   rw_assembly.rst
   rw_control.rst
   rw_pathoptimization.rst
   rw_pathplanners.rst
   rw_proximitystrategies.rst
   rw_simulation.rst
   rw_task.rst
   rws.rst
   rwsim.rst
